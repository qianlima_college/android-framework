![输入图片说明](01.png)

![输入图片说明](02.png)

![输入图片说明](input-14.png)


[试看连接：https://ke.qq.com/course/package/51285?tuin=7d4eb354](https://ke.qq.com/course/package/51285?tuin=7d4eb354)
课程1（入门） Android手机大厂入职培训Framework系统高级开发课

01
Android手机大厂入职培训课介绍
【录播】Android手机大厂入职培训课介绍(26分钟) 免费试学

02
Android源码Aosp环境搭建
【录播】Android源码Aosp环境搭建(22分钟) 免费试学

03
Android源码环境搭建补充
【录播】jack-server错误的分析及解决(12分钟) 免费试学

04
运行自己Android系统第一行代码
【录播】运行自己Android系统第一行代码(21分钟)

05
Android studio上导入源码及一些系统基础知识
【录播】Android Studio导入源码及一些系统基础知识(11分钟)

06
Android系统需求开发的分析步骤
【录播】Android系统需求开发的分析步骤(23分钟)

07
Android系统中调试追踪方法及相关命令
【录播】Android系统中调试追踪方法及相关命令(15分钟)

08
Activity启动流程分析
【录播】Activity启动流程分析(36分钟)

09
实战项目去除第三方应用广告页面
【录播】实战项目去除第三方应用广告页面(38分钟)

10
实战项目去除第三方应用广告页面-方法2
【录播】实战项目去除第三方应用广告页面方法2(21分钟)

11
Window上制作自己的游戏模拟器1
【录播】Window上制作自己的游戏模拟器1(12分钟)

【录播】入职体验课简单介绍(12分钟)

12
Window上制作自己的游戏模拟器2
【录播】Window上制作自己的游戏模拟器2(15分钟)

13
Android系统内置系统应用开发介绍
【录播】Android系统内置系统应用开发介绍(22分钟)

14
Android系统内置系统应用开发实战1
【录播】Android系统内置系统应用开发实战1(16分钟)

15
Android系统内置系统应用开发实战2
【录播】Android系统内置系统应用开发实战2(10分钟)



课程2（实战） Android高级架构师/系统源码/Framework实战系列

01
课程简介及学习思路
【录播】课程简介及学习思路(18分钟)

02
android开机动画深入开发专题简介
【录播】android开机动画深入开发专题简介(15分钟) 免费试学

03
android开机动画BootAnimation启动源码分析
【录播】android开机动画BootAnimation启动源码分析(27分钟)

04
android开机动画BootAnimation结束流程分析
【录播】android开机动画BootAnimation结束流程分析(38分钟)

05
android开机动画BootAnimation的opengl绘制源码分析
【录播】BootAnimation的opengl绘制源码分析(18分钟)

06
android开机动画BootAnimation实战使用opengl绘制时间
【录播】BootAnimation实战使用opengl绘制时间(23分钟)

07
android开机动画BootAnimation源码分析zip包的运行原理
【录播】BootAnimation源码分析zip包的运行原理(23分钟)

08
android开机动画BootAnimation的实战制作不一样的zip包动画
【录播】BootAnimation的实战制作不一样的zip包动画(21分钟)

09
android native层多线程实现之posix方案
【录播】android native层多线程实现之posix方案(33分钟) 免费试学

10
android native层自带多线程类Threads-1
【录播】android native层自带多线程类Threads-1(13分钟)

11
android native层自带多线程类Threads-2
【录播】android native层自带多线程类Threads-2(19分钟)

12
android native层的堆栈辅助调试打印方法
【录播】android native层的堆栈辅助调试打印方法(19分钟)

13
android系统Zygote启动源码分析1
【录播】android系统Zygote启动源码分析1(7分钟)

14
android系统Zygote启动源码分析2
【录播】android系统Zygote启动源码分析2(42分钟) 免费试学

15
android framework之Zygote fork新进程源码及实战开发
【录播】Zygote fork新进程源码及实战开发(34分钟)

16
android系统SystemServer启动源码分析1
【录播】android系统SystemServer启动源码分析1(23分钟)

17
android系统SystemServer启动源码分析2
【录播】android系统SystemServer启动源码分析2(6分钟)

18
android系统SystemServer启动源码分析3
【录播】android系统SystemServer启动源码分析3(19分钟)

19
android系统SystemServer启动源码分析4
【录播】android系统SystemServer启动源码分析4(13分钟)

20
android系统SystemServer启动源码分析5
【录播】android系统SystemServer启动源码分析5(23分钟)

21
android系统HomeActivity进程启动源码分析1
【录播】android系统HomeActivity进程启动源码分析1(35分钟)

22
android系统HomeActivity进程启动源码分析2
【录播】android系统HomeActivity进程启动源码分析2(17分钟)

23
android系统HomeActivity进程启动源码分析3
【录播】android系统HomeActivity进程启动源码分析3(7分钟)

24
android系统AMS与Zygote通信及启动FallbackHome源码分析1
【录播】AMS与Zygote通信及启动FallbackHome(24分钟)

25
android系统AMS与Zygote通信及启动FallbackHome源码分析2
【录播】AMS与Zygote通信及启动FallbackHome2(17分钟)

26
android系统之Fallback的结束到Launcher启动源码分析
【录播】FallbackHome的结束到Launcher启动源码分析(36分钟)


binder通信课程介绍
【录播】跨进程通信之课程介绍(26分钟) 免费试学

【录播】跨进程通信之课程介绍2(15分钟) 免费试学

02
binder中级水平学习
【录播】binder使用方式及常见组成及案例分析(20分钟)

【录播】binder使用方式及常见组成及案例分析2(14分钟)

【录播】binder的aidl工具生成java源码分析1(12分钟)

【录播】binder的aidl工具生成java源码分析2(5分钟)

【录播】binder的aidl工具生成java源码分析3(13分钟)

【录播】oneway，in，out关键字介绍(39分钟)

【录播】binder双向通信(44分钟)

【录播】binder的linktodeath相关(14分钟)

【录播】binder的messenger相关(23分钟) 免费试学

03
android系统socket深入学习
【录播】socket课程介绍(9分钟) 免费试学

【录播】c++中socket通信的demo讲解及实战1(47分钟)

【录播】c++中socket通信的demo讲解及实战2(10分钟)

【录播】c++与java的socket通信demo讲解及实战1(26分钟)

【录播】c++与java的socket通信demo讲解及实战2(5分钟)

【录播】epoll的讲解及实战demo(21分钟)

【录播】epoll的讲解及实战demo2(19分钟)

【录播】epoll的讲解及实战demo3(23分钟)

【录播】socketpair讲解(18分钟) 免费试学

【录播】android系统中的socket通信案例分析(25分钟)

04
实战一个socket通信实现命令行执行程序
【录播】实战一个socket通信实现命令行执行程序1(27分钟)

【录播】实战一个socket通信实现命令行执行程序2(23分钟)

05
binder的native层面实战源码分析
【录播】应用程序binder启动源码分析1(14分钟)

【录播】应用程序binder启动源码分析2(11分钟)

【录播】ServiceManager启动源码分析1(24分钟)

【录播】ServiceManager启动源码分析1(15分钟)

【录播】c++程序之间binder通信实战案例1(15分钟)

【录播】c++程序之间binder通信实战案例2(12分钟)

【录播】c++与java程序之间binder通信实战案例1(14分钟)

【录播】c++与java程序之间binder通信实战案例2(9分钟)

06
binder的native层面实战源码分析
【录播】binder通信java及jni部分源码分析1(41分钟)

【录播】binder通信java及jni部分源码分析2(31分钟)

【录播】binder通信java及jni部分源码分析3(34分钟)

07
binder的驱动层面实战源码分析
【录播】binder驱动的简单核心方法及原理介绍(24分钟)

【录播】binder_open及mmap介绍1(25分钟)

【录播】binder_open及mmap介绍2(32分钟)

【录播】binder驱动的数据写入传递分析1(8分钟)

【录播】binder驱动的数据写入传递分析2(45分钟)

【录播】binder驱动的数据写入传递分析3(44分钟)

【录播】binder驱动中不同进程的等待与唤醒分析(21分钟)

【录播】binder驱动的读取分析(27分钟)

【录播】binder系统应用层和驱动层打通总结(51分钟)

08
binder的实战企业级案例，系统binder的bug分析及解决
【录播】binder的问题log抓取(15分钟)

【录播】log分析及bug修复1(17分钟)

【录播】log分析及bug修复2(20分钟)



课程4 Android手机大厂Framework系统-Input系统专题实战课

01
input课程背景及收获
【录播】input课程背景及收获(15分钟) 免费试学

02
input课程内容课表介绍
【录播】input课程内容课表介绍(20分钟)

03
android源码环境下debug
【录播】android源码环境下debug(11分钟)

04
input课程环境准备
【录播】input课程环境准备(15分钟) 免费试学

05
getevent详细讲解
【录播】getevent详细讲解(55分钟) 免费试学

06
多指触控协议深入分析
【录播】多指触控协议深入分析(26分钟) 免费试学

07
InputManager启动源码分析
【录播】InputManager启动源码分析(31分钟)

08
inputreader源码分析1
【录播】inputreader源码分析1(55分钟)

09
inputreader源码分析2
【录播】inputreader源码分析2(8分钟)

10
inputreader源码分析3
【录播】inputreader源码分析3(15分钟)

11
inputreader源码分析4
【录播】inputreader源码分析4(22分钟)

12
inputreader源码分析5
【录播】inputreader源码分析5(34分钟)

13
inputreader中数据加工1
【录播】inputreader中数据加工1(37分钟)

14
inputreader中数据加工2
【录播】inputreader中数据加工2(13分钟)

15
inputreader总结部分
【录播】inputreader总结部分(34分钟)

16
项目实战之动态控制触摸功能
【录播】项目实战之动态控制触摸功能(22分钟)

17
事件分发源码分析1
【录播】事件分发源码分析1(52分钟)

18
事件分发源码分析2
【录播】事件分发源码分析2(43分钟)

19
事件分发源码分析3
【录播】事件分发源码分析3(34分钟)

20
事件分发源码分析4
【录播】事件分发源码分析4(13分钟)

21
事件分发源码分析5
【录播】事件分发源码分析5(33分钟)

22
事件分发源码分析6
【录播】事件分发源码分析6(27分钟)

23
key事件拦截分析1
【录播】key事件拦截分析1(21分钟)

24
key事件拦截分析2
【录播】key事件拦截分析2(16分钟)

25
触摸事件分离分析1
【录播】触摸事件分离分析1(21分钟)

26
触摸事件分离分析2
【录播】触摸事件分离分析2(10分钟)

27
dumpsys分析input
【录播】dumpsys分析input(21分钟)

28
anr产生源码分析
【录播】anr产生源码分析(30分钟)

29
anr实例分析1
【录播】anr实例分析1(25分钟)

30
anr实例分析2
【录播】anr实例分析2(22分钟)

31
触摸轨迹源码分析
【录播】触摸轨迹源码分析(25分钟)

32
触摸小圆源码分析
【录播】触摸小圆源码分析(27分钟)

33
实战项目-过滤某个窗口不接受触摸
【录播】实战项目-过滤某个窗口不接受触摸(24分钟)

34
实战项目-触摸事件注入1
【录播】实战项目-触摸事件注入1(32分钟)

35
实战项目-触摸事件注入2
【录播】实战项目-触摸事件注入2(23分钟)

36
项目实战-没有systemserver情况下触摸识别
【录播】项目实战-没有systemserver情况下触摸识别(35分钟)

37
项目实战-后台监听触摸事件
【录播】项目实战-后台监听触摸事件(22分钟)